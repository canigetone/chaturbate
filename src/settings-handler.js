
"use strict";

class SettingsHandler {
    constructor(api) {
        this.api = api;
        this.settings = [];
    }

    register(object) {
        let settings = object.settings;
        if (!settings || settings.constructor !== Array || settings.length === 0) {
            return this;
        }

        this.settings.push({
          name: 'settings_' + object.name.replace(/\s+/g, '') + '_header',
          type: 'choice',
          required: false,
          label: ':'.repeat(20) + ' ' + object.name.toUpperCase() + ' ' + ':'.repeat(20)
        });
        for (let i = 0; i < settings.length; i++) {
          this.settings.push(settings[i]);
        }

        return this;
    }

    fetch() {
        return this.settings;
    }
}

module.exports = SettingsHandler;
