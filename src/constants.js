
module.exports = {
    chars: {
        hearts: ':heart2',
        hearts2: ':Hearts2',
        glitter: ':pixelglitter',
        flowers: ':tinyflower2',
        bow: ':bluebow',
        smiley: ':smile',
        text_queen: String.fromCodePoint(0x2655),
        text_queen_full: String.fromCodePoint(0x265B),
        text_heart: String.fromCodePoint(0x2665),
        text_diamond: String.fromCodePoint(0x2666),
        text_star: String.fromCodePoint(0x2605),
        text_star_pointed: String.fromCodePoint(0x2726),
        text_right: String.fromCodePoint(0x25BA),
        text_left: String.fromCodePoint(0x25C0),
        text_right_small: String.fromCodePoint(0x25B8),
        text_left_small: String.fromCodePoint(0x25C2),
        text_stopwatch: String.fromCodePoint(0x23F0),
        text_clock: String.fromCodePoint(0x23F1),
        text_hourglass: String.fromCodePoint(0x231B),
        text_hourglass_flowing: String.fromCodePoint(0x23F3),
        text_dotted_line: String.fromCodePoint(0x2999),
    },

    colors: {
        black: '#000',
        white: '#fff',
        cream: '#f9f6ed',

        fan_green: '#e6f9e6',
        mod_red: '#ffe7e7',
        help: '#06140d',
        help_bg: '#f2ffe6',
        tipnote_yellow: '#ffff00',
        legend_purple: '#e8f4f8',
        legend_green: '#f3ecf9',

        timer_blue: '#add8e6',
        timer_magenta: '#ffa6ff',

        light_grey: '#e6e6e6',
        grey: '#aaa',
        dark_grey: '#333',

        light_aqua: '#adeaea',
        dark_aqua: '#006767',

        light_red: '#ff9a9a',
        red: '#f4c1bc',
        dark_red: '#cc0000',

        light_orange: '#ffd9b3',
        orange: '#F65F00',
        dark_orange: '#e77400',

        light_yellow: '#ffff94',
        yellow: '#f4d599',
        dark_yellow: '#E6D200',

        light_green: '#94e594',
        green: '#00B300',
        dark_green: '#006600',

        light_blue: '#d1eaee',
        blue: '#0099ff',
        dark_blue: '#0629AC',

        light_purple: '#f2cdff',
        purple: '#e2a2ea',
        dark_purple: '#3d003d',

        light_pink: '#FFE6EA',
        pink: '#FF00EE',
        dark_pink: '#FF6680'
    }
};
