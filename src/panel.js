
"use strict";

class Panel {
    constructor(user) {
        this.user = user;
        this.templates = [
            '3_rows_of_labels',
            '3_rows_11_21_31',
            '3_rows_12_21_31',
            '3_rows_12_22_31',
            'image_template'
        ];
        this.response = {
            template: this.templates[0],
            row1_label: '',
            row1_value: '',
            row2_label: '',
            row2_value: '',
            row3_label: '',
            row3_value: ''
        };
    }

    setPanel(panel) {
        this.response = panel;
    }

    getResponse() {
        return this.response;
    }
}

module.exports = Panel;
