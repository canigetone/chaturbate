
"use strict";

class Abstract {
    constructor() {
        this.api = null;
        this.name = '';
        this.settings = [];
        this.commands = {};
    }

    setDependencies(api) {
        this.api = api;
    }

    getStatus() {
      return `${this.name} - Available commands:`;
    }
}

module.exports = Abstract;
