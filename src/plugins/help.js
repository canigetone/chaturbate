
"use strict";

var Abstract  = require('./abstract'),
    Constants = require('./../constants');

class Help extends Abstract {
    constructor() {
        super();
        this.name = 'Help';
        this.commands = {
            help: {
                scope: this,
                handler: this.displayHelp
            }
        };
        this.descriptions = [];
    }

    discover(plugins) {
        for (let index in plugins) {
            if (!plugins[index].commands)
                continue;
            this.iterateCommands(plugins[index].commands);
        }
    }

    iterateCommands(commands) {
        var index;
        for (index in commands) {
            if (!commands.hasOwnProperty(index))
                continue;
            this.appendHelpMessage(index, commands[index]);
        }
    }

    appendHelpMessage(name, command) {
        if (!command.description)
            return;
        this.descriptions.push({
            message: `/${name}: ${command.description}`,
            access: command.access
        });
    }

    displayHelp(user, command) {
        var index,
            description,
            output = [];

        for (index in this.descriptions) {
            description = this.descriptions[index];
            if (user.hasPermission(description.access))
                output.push(description.message);
        }

        if (output.length < 1)
            return;

        this.api.sendNotice(
            output.join("\n"),
            user.name,
            Constants.colors.help_bg,
            Constants.colors.help,
            'bold');
    }
}

module.exports = Help;
