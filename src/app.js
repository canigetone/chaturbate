
"use strict";

let Bot = require('./bot');

class App extends Bot {
    constructor(api) {
        super(api);
        this.methods.push('onDrawPanel');
    }

    onDrawPanel(user, panel) {
        this.callPlugins.call(this, 'onDrawPanel', [user, panel]);
        return panel.getResponse();
    }
}

module.exports = App;
