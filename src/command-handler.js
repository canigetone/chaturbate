
"use strict";

let Constants = require('./constants'),
    InvalidCommand   = require('./error/invalidcommand'),
    InvalidArgument  = require('./error/invalidargument'),
    PermissionDenied = require('./error/permissiondenied');

class CommandHandler {
    constructor(api) {
        this.api = api;
        this.commands = {};
        this.subcommands = {};
    }

    register(object) {
        if (!object.commands) {
            return this;
        }

        for (let command in object.commands) {
            if (this.commands.hasOwnProperty(command)) {
                throw new InvalidCommand("Specified command '" + command + "' conflicts with an existing command.");
            }
            if (!object.commands[command].scope) {
                object.commands[command].scope = object;
            }
            this.commands[command] = object.commands[command];

            this.subcommands[command] = {
                command_names: [],
                handlers: {},
                help: []
            };

            if (!object.commands[command].hasOwnProperty('subcommands'))
                continue;

            for (let subcommand of object.commands[command].subcommands) {
                this.subcommands[command].help.push(`${subcommand.names.join('|')} : ${subcommand.description}`);
                for (let name of subcommand.names) {
                    this.subcommands[command].command_names.push(name);
                    Object.assign(this.subcommands[command].handlers, {[name]: {handler: subcommand.handler,
                                                                                success: subcommand.success}});
                }
            }
        }

        return this;
    }

    handle(user, message) {
        try {
            let command = message.getCommand();
            let cmd = command;
            if (!this.commands[command])
                return message.getResponse();
            // throw new InvalidCommand("Specified command '" + command + "' is not valid.");

            command = this.commands[command];
            if (command.access && !user.hasPermission(command.access))
                throw new PermissionDenied('You do not have permission to use that command');

            let params = [];
            if (command.params)
                params = message.getCommandParameters(command.params);

            let ret = true;
            if (command.hasOwnProperty('subcommands')) {
                params = params.split(' ');
                let subcommand = params.shift();
                if (subcommand === '')
                    this.api.sendNotice(
                        (typeof command.scope.getStatus === 'function' ? command.scope.getStatus() + '\n' : '') +
                        this.subcommands[cmd].help.join('\n'),
                        user.name,
                        Constants.colors.help_bg,
                        Constants.colors.help,
                        'bold');
                else if (this.subcommands[cmd].command_names.includes(subcommand) &&
                    typeof this.subcommands[cmd].handlers[subcommand].handler === 'function') {
                    ret = this.subcommands[cmd].handlers[subcommand].handler.apply(command.scope, [user, params]);
                    if (ret === undefined || ret === true)
                        this.api.sendNotice(this.subcommands[cmd].handlers[subcommand].success,
                                            user.name,
                                            Constants.colors.light_green,
                                            Constants.colors.dark_green,
                                            'bold');
                } else
                    this.api.sendNotice('Command not implemented...', user.name, Constants.colors.light_yellow);
            } else
                ret = command.handler.apply(command.scope, [user].concat(params));
            if (ret !== undefined && ret !== true)
                throw new InvalidArgument(`Failed: ${ret}`);
        } catch (error) {
            this.api.sendNotice(error.toString(), user.name, Constants.colors.light_red, Constants.colors.black, 'bold');
        } finally {
            return message.getResponse();
        }
    }
}

module.exports = CommandHandler;
