
"use strict";

class RuntimeError extends Error
{
}

module.exports = RuntimeError;
