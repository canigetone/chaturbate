
"use strict";

class InvalidCommandError extends Error
{
}

module.exports = InvalidCommandError;
