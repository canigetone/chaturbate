
"use strict";

let User = require('./user'),
    Panel = require('./panel'),
    Message = require('./message');

class EventProxy {
    constructor(api) {
        this.api = api;
        this.intercepts = {};
    }

    initialise(methods, object) {
        for (let index in methods) {
            let method = methods[index],
                handler = object[method];
            if (typeof handler === 'function')
                this.api[method](this.proxy(method, object, handler));
        }

        let user = User.createFromUsername(this.api.room_slug, this.api.room_slug);
        object.onStart(user);
    }

    proxy(method, scope, handlerFn) {
        if (!this[method]) {
            return function() {
                this.api.log('Proxied ' + method);
                return handlerFn.apply(scope, arguments);
            };
        }

        return function () {
            let proxied = this[method].apply(this, arguments);
            if (typeof this.intercepts[method] === 'function') {
                handlerFn = this.intercepts[method].apply(scope, [handlerFn].concat(proxied));
            }
            // this.api.log('Proxied intercepted ' + method);
            return handlerFn.apply(scope, proxied);
        }.bind(this);
    }

    onEnter(cbUser) {
        let user = User.createFromMessage(cbUser, this.api.room_slug);
        return [user];
    }

    onMessage(cbMessage) {
        let user = User.createFromMessage(cbMessage, this.api.room_slug),
            message = new Message(cbMessage.m, user, cbMessage);
        return [user, message];
    }

    onTip(cbTip) {
        let from    = User.createFromTip(cbTip, this.api.room_slug),
            to      = User.createFromUsername(cbTip.to_user, this.api.room_slug),
            message = Message.createFromTip(cbTip, from);
        return [from, to, Number.parseInt(cbTip.amount, 10), message];
    }

    onLeave(cbUser) {
        let user = User.createFromMessage(cbUser, this.api.room_slug);
        return [user];
    }

    onDrawPanel(cbUser) {
        let user = User.createFromMessage(cbUser, this.api.room_slug);
        let panel = new Panel(user);
        return [user, panel];
    }
}

module.exports = EventProxy;
