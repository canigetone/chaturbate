
"use strict";

let EventProxy = require('./event-proxy'),
    CommandHandler = require('./command-handler'),
    SettingsHandler = require('./settings-handler'),
    HelpPlugin = require('./plugins/help');

class Bot {
    constructor(api) {
        this.api = api;
        this.methods = ['onEnter', 'onMessage', 'onTip', 'onLeave'];

        this.commands = new CommandHandler(this.api);
        this.settings = new SettingsHandler(this.api);

        this.plugins = [];
        this.help = new HelpPlugin();
        this.register(this.help);

        this.callPlugins = function(method, args) {
            let index, plugin;
            for (index in this.plugins) {
                plugin = this.plugins[index];
                if (typeof plugin[method] === 'function') {
                    plugin[method].apply(plugin, args);
                }
            }
        };
    }

    run() {
        this.help.discover(this.plugins);
        this.api.settings_choices = this.settings.fetch();

        let proxy = new EventProxy(this.api);
        proxy.initialise(this.methods, this);
    }

    register(plugin) {
        if (typeof plugin.setDependencies === 'function') {
            plugin.setDependencies(this.api);
        }
        this.commands.register(plugin);
        this.settings.register(plugin);
        this.plugins.push(plugin);
        return this;
    }

    onStart(user) {
        this.callPlugins.call(this, 'onStart', [user]);
    }

    onEnter(user) {
        this.callPlugins.call(this, 'onEnter', [user]);
    }

    onMessage(user, message) {
        if (message.isCommand()) {
            message.hide();
            this.commands.handle(user, message);
        } else {
            this.callPlugins.call(this, 'onMessage', [user, message]);
        }
        return message.getResponse();
    }

    onTip(from, to, amount, message) {
        this.callPlugins.call(this, 'onTip', [from, to, amount, message]);
    }

    onLeave(user) {
        this.callPlugins.call(this, 'onLeave', [user]);
    }
}

module.exports = Bot;
