# Chaturbate framework

> This code is not officially supported by or affiliated with chaturbate.com.

This framework provides components to create Apps and Bots, removing common boiler plate code and increasing the speed
and reliability of development. Based on https://github.com/falconzs/cb-framework

## Getting Started

See https://gitlab.com/canigetone/appbot
